var mongoose = require("mongoose");
var messageScheme = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'الرجاء إدراج رابط الإسم رباعي ..  ']            
  },
  issuesDate: { type: Date, default: Date.now },
  message: {
    type: String ,
    required: [true, 'الرجاء إدراج الرسالة ..  ']                
  }, 
  email: {
    type: String,
    required: [true, 'الرجاء إدراج البريد الإلكتروني ..  ']                    
  }
});
module.exports = mongoose.model("message", messageScheme);
