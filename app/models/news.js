var mongoose = require("mongoose");
var newsSheme = mongoose.Schema({
  title: {
    type: String,
    required: [true, 'الرجاء إدراج عنوان الخبر ..  ']                        
  },
  image: {
    type: String
  },
  decription: {
    type: String,
    required: [true, 'الرجاء إدراج تفاصيل الخبر ..  ']                            
  }, 
  postDate: { type: Date, default: Date.now },
});
module.exports = mongoose.model("news", newsSheme);
