﻿var mongoose = require("mongoose");

var subscriptionScheme = mongoose.Schema({
    name: {
        type: String,
        requird: [true, 'الرجاء إدراج اسم المستخدم']
    },

    email: {
        type: String,
        unique: true,
        lowercase: true,
        required: [true, 'الرجاء إدراج البريد الإلكتروني ..  ']
    },

    mobile: String,

    address: {
        type: String,
        required: [true, 'الرجاء كتابه عنوانك ..  ']
    },

    district: String,

    birthdate: {
        type: Date
    },

    //Reason of Join	
    description: {
        type: String,
        required: [true, 'الرجاء إدراج أسباب الانضمام ..  ']
    },

    //How Do You Know About H. S. Academy?	
    sourceKnowing: {
        type: String,
        required: [true, 'الرجاء تحديد كيفيه معرفتك بالأكادميه  ..  ']
    },
    status: {
        type: String,
        default: 'pending',
        enum: ['pending','rejected','accepted']
    }

});

module.exports = mongoose.model("subscriptions", subscriptionScheme);
