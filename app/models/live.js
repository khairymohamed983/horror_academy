var mongoose = require("mongoose");
var liveScheme = mongoose.Schema({
    title: {
        type: String,
        required: [true, 'الرجاء إدراج نبذة عن البث المباشر ..  ']
    },
    url: {
        type: String,
        required: [true, 'الرجاء إدراج رابط البث المباشر ..  ']        
    },
    bodcastDate: { type: Date, default: Date.now },
    isCurrent: { type: Boolean, default: true }
});
module.exports = mongoose.model("live", liveScheme);
