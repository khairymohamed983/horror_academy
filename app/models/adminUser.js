var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

var adminUser = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true,
        required: [true, 'الرجاء إدراج البريد الإلكتروني    ']
    },
    title: String,
    profileImage: String,
    displayName: {
        type: String,
        required: [true, ' الرجاء إدراج كلمة المرور  ..  ']
    },
    password: {
        type: String,
        select: false,
        required: [true, ' الرجاء إدراج كلمة المرور  ..  ']
    }
});

adminUser.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
            user.password = hash;
            next();
        });
    });
});

adminUser.methods.comparePassword = function (password, done) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
        done(err, isMatch);
    });
};

module.exports = mongoose.model('adminUser', adminUser);