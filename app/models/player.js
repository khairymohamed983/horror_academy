var mongoose = require("mongoose");
var playerSheme = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'الرجاء إدراج إسم اللاعب ..  ']            
  },
  image: {
    type: String,
    required: [true, 'الرجاء إدراج صورة اللاعب ..  ']                
  },
  position:{
    type: String,
    required: [true, 'الرجاء إدراج مركز اللاعب ..  ']                
  },
  decription: {
    type: String
  }, 
  birthDate: { type: Date, default: Date.now },
  subscriptionDate: { type: Date, default: Date.now },
});
module.exports = mongoose.model("players", playerSheme);
