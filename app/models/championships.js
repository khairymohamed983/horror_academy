var mongoose = require("mongoose");
var championshipSheme = mongoose.Schema({
  title: {
    type: String , 
    required: [true, ' الرجاء إدراج اسم البطولة  ..  ']
    
  },
  image: {
    type: String
  },
  decription: {
    type: String
  }, 
  year: {
    type: String,
    required: [true, ' الرجاء إدراج عام الحصول علي البطولة  ..  ']
  }
});
module.exports = mongoose.model("championship", championshipSheme);
