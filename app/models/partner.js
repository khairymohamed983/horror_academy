var mongoose = require("mongoose");
var partnerScheme = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'الرجاء إدراج الإسم ..  ']
    },
    title: {
        type: String ,
        required: [true, 'الرجاء إدراج الصفة ..  ']        
    },
    image: {
        type: String
    },
    decription: {
        type: String
    },
});
module.exports = mongoose.model("partner", partnerScheme);
