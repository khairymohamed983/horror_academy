var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

var user = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true,
        required: [true, 'الرجاء إدراج البريد الإلكتروني ..  ']
    },
    name: {
        type: String,
        required: [true, 'الرجاء إدراج اسم المستخدم ..  ']
    },
    profileImage: String,
    mobile: String,
    address: String,
    region: String,
    howknow: String,
    latestActivationCode: String,
    isActivated:  {
        type: Boolean,
        default : false
    },
    password: {
        type: String,
        select: false,
        required: [true, 'الرجاء إدراج كلمة المرور ..  ']
    }
});


user.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
            user.password = hash;
            next();
        });
    });
});

user.methods.comparePassword = function (password, done) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
        done(err, isMatch);
    });
};

module.exports = mongoose.model('user', user);