var mongoose = require("mongoose");
var aboutScheme = mongoose.Schema({
  vission: {
    type: String,
    required: [true, 'الرجاء إدراج الرؤية ']
  },
  goal: {
    type: String,
    required: [true, ' الرجاء إدراج الهدف ']
  },
});
module.exports = mongoose.model("about", aboutScheme);
