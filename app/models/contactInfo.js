var mongoose = require("mongoose");
var contactScheme = mongoose.Schema({
  address: {
    type: String
  },
  phone: {
    type: String
  },
  whatsApp: {
    type: String
  },
  twitter: {
    type: String
  },
  instagram: {
    type: String
  },
  snapshat: {
    type: String
  },
  image: {
    type: String
  }

});
module.exports = mongoose.model("contactInfo", contactScheme);
