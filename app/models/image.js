var mongoose = require("mongoose");
var imageScheme = mongoose.Schema({
  title: {
    type: String , 
    required: [true, 'الرجاء إدراج نبذة عن الصورة ..  ']    
  },
  image: {
    type: String , 
    required: [true, 'الرجاء إدراج الصورة ..  ']        
  },
  decription: {
    type: String
  }, 
});
module.exports = mongoose.model("images", imageScheme);
