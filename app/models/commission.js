var mongoose = require("mongoose");
var commisionSheme = mongoose.Schema({
  name: {
    type: String , 
    required: [true, ' الرجاء إدراج اسم اللجنة  ..  ']
  },
  image: {
    type: String
  },
  decription: {
    type: String
  },
  members: [
    {
        memberName: {
            type: String, 
            //just amount of time 
          //required: [true, ' الرجاء إدراج اسم العضو  ..  ']
        },
        position: {
          type: String , 
          //required: [true, ' الرجاء إدراج منصب العضو  ..  ']
        },
        title:String,
        image:String,
        description:String,
    }
  ]
});
module.exports = mongoose.model("commisions", commisionSheme);
