var mongoose = require("mongoose");
var joinusScheme = mongoose.Schema({
  name: {type: String},
  email: {type: String},
  mobile: {type: String},
  address: {type: String},
  region: {type: String},
  howknow: {type: String},
  birthdate: {type: String},
  reason: {type: String}
});
module.exports = mongoose.model("joinus", joinusScheme);
