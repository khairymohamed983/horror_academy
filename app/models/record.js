var mongoose = require("mongoose");
var recordScheme = mongoose.Schema({
    noGoals: Number,
    noPlayers: Number,
    noEvants: Number,
    noPrize: Number,

    players: [{
        image: String,
        name: String,
        category: String,
        reason: String        
    }]



});
module.exports = mongoose.model("record", recordScheme);
