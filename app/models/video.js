var mongoose = require("mongoose");
var videoScheme = mongoose.Schema({
  title: {
    type: String
  },
  videoUrl: {
    type: String
  },
  decription: {
    type: String
  }, 
  isYoutube: {
    type: Boolean
  },
});
module.exports = mongoose.model("videos", videoScheme);
