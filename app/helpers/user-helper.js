function makeActivationCode() {
    var text = "";
    var possible = "ABC64646DEFGH4548787IJKL545488M998NOPQRST32212UVWXYZ54554abcdefgh1122123ijklmno5564812133311pqrst4545uvwxyz0123456789";
    for (var i = 0; i < 6; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function sendEmail(email, name, activationCode) {
    // var sendmail = require('sendmail')({ silent: true, devPort: 1025 })
    var sendmail = require('sendmail')({
        smtpHost:'localhost',
        smtpPort: 12301
    })
     
    sendmail({
        from: 'khairymohamed983@gmail.com',
        to: email,
        subject: 'كود تفعيل تطبيق أكاديمية فرقة الرعب',
        html: `عزيزنا ${name}  إليك كود التفعيل الخاص بك لتنشيط حسابك في تطبيق الأكاديمية ${activationCode}`
    }, function (err, reply) {
        console.log(err && err.stack)
        console.dir(reply)
    })

}