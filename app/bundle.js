// bundle all API's Here 
module.exports = function () {
    const express = require('express');
    const router = express.Router();
    var ensureAuthenticated = require('./midleware/authMiddleware').authMiddleware;
    require('./api/users-api')(router, ensureAuthenticated);
    require('./api/championships-api')(router, ensureAuthenticated);
    require('./api/comissions-api')(router, ensureAuthenticated);
    require('./api/conatacts-api')(router, ensureAuthenticated);
    require('./api/gallary-api')(router, ensureAuthenticated);
    require('./api/news-api')(router, ensureAuthenticated);
    require('./api/subscriptions-api')(router, ensureAuthenticated);
    require('./api/other-api')(router, ensureAuthenticated);
    require('./api/partners')(router, ensureAuthenticated);
    require('./api/players-api')(router, ensureAuthenticated);
    require('./api/records-api')(router, ensureAuthenticated);    
    return router;
}
