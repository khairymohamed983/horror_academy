///  Seed Data here after connecting to the Database

const adminUser = require('./models/adminUser')
const about = require('./models/about');
const contact = require('./models/contactInfo');

module.exports = function () {
    console.log('Chacking Required Fields that must be in database before app launching .... ');
    console.log('=============================================================================');
    console.log('Checking basic admin user ....')
    // Insert Default User if Not Exist
    adminUser.find({
        email: 'admin'
    }, function (err, docs) {
        if (err) {
            console.log(err);
            return;
        }
        if (!docs.length) {
            console.log('there is no user adding the basic admin user ...... ');
            let admin = new adminUser({
                displayName: 'Admin',
                email: 'admin',
                password: '123456'
            });
            admin.save(function (err) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            console.log('Ok user Exsists ...... ');
        }
    });
    console.log('=============================================================================');
    console.log('Checking about us static content ....')
    // Insert Default User if Not Exist
    about.find({}, function (err, docs) {
        if (err) {
            console.log(err);
            return;
        }
        if (!docs.length) {
            console.log('there is no aboutus data in the database ...... ');
            let a = new about({
                vission: 'الرؤية',
                goal: 'الهدف',
                other: 'اخري'
            });
            a.save(function (err) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            console.log('Ok aboutus content is exist ...... ');
        }
    });

    console.log('=============================================================================');
    console.log('Checking contactus static content ....')
    // Insert Default User if Not Exist
    contact.find({}, function (err, docs) {
        if (err) {
            console.log(err);
            return;
        }
        if (!docs.length) {
            console.log('there is no contactus data in the database ...... ');
            let a = new contact({
                address:' ',
                phone: '',
                whatsApp: '',
                twitter: '',
                instagram: '',
                snapshat:'',
            });
            a.save(function (err) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            console.log('Ok contactus content is exist ...... ');
        }
    });


}
