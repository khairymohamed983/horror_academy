const players = require('../models/player');

module.exports = function (router, ensureAuthenticated) {
    router.get('/players/newall', function (req, res) {

        let perPage = req.param('perPage');;
        let  page = req.param('page');

        let ammount=parseInt(perPage)*parseInt(page);
        var query = players.find().skip(ammount).limit(parseInt(perPage));
        query.exec(function (err, docs) {
            res.send({
                players: docs,
                page: page
            });
        });

    });


    router.get('/players/all', function (req, res) {
        players.find({}, function (err, docs) {
            res.send(docs);
        });
    });


  


    router.post('/players/add', ensureAuthenticated, function (req, res) {
        players.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/players/update', ensureAuthenticated, function (req, res) {
        players.findById(req.body._id, function (err, doc) {
            if (err) {
                return res.status(500).send(err);
            }
            players.updateOne(doc, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    router.post('/players/delete', ensureAuthenticated, function (req, res) {
        players.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });

}
