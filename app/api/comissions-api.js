const commisions = require('../models/commission');

module.exports = function (router, ensureAuthenticated) {
    router.get('/commisions/all', function (req, res) {
        commisions
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });


    router.get('/commisions/newall', function (req, res) {

        let perPage = req.param('perPage');;
        let  page = req.param('page');

        let ammount=parseInt(perPage)*parseInt(page);
        var query = commisions.find().skip(ammount).limit(parseInt(perPage));
        query.exec(function (err, docs) {
            res.send({
                commisions: docs,
                page: page
            });
        });

    });


    router.post('/commisions/add', ensureAuthenticated, function (req, res) {
        commisions.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/commisions/update', ensureAuthenticated, function (req, res) {
        commisions.findById(req.body._id, function (err, ch) {
            if (err) {
                return res.status(500).send(err);
            }
            commisions.updateOne(ch, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    router.post('/commisions/delete', ensureAuthenticated, function (req, res) {
        commisions.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });

}
