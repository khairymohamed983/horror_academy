var moment = require('moment');
var jwt = require('jwt-simple');
var bcrypt = require('bcryptjs');
var createJWT = require('../midleware/authMiddleware').jwtgen;

var User = require('../models/user')
var AdminUser = require('../models/adminUser')
const UserHelper = require('../helpers/user-helper');

/// Users End Point
module.exports = function (router, ensureAuthenticated) {

    router.post('/users/login', function (req, res) {
        AdminUser.findOne({
            email: req.body.email
        }, '+password', function (err, user) {

            if (!user) {
                return res.status(401).send({
                    message: 'Invalid email and/or password'
                });
            }

            user.comparePassword(req.body.password, function (err, isMatch) {
                if (!isMatch) {
                    return res.status(401).send({
                        message: 'Invalid password'
                    });
                }
                res.send({
                    token: createJWT(user)
                });
            });
        });
    });

    // mobile login 
    router.post('/users/moblogin', function (req, res) {
        User.findOne({
            email: req.body.email
        }, '+password', function (err, user) {
            if (!user) {
                return res.status(401).send({
                    message: 'البريد الإلكتروني لا ينتمي لأي حساب مسجل لدينا ان لم تمتلك حسابا قم بالتسجيل'
                });
            }

            user.comparePassword(req.body.password, function (err, isMatch) {
                if (!isMatch) {
                    return res.status(401).send({
                        message: 'كلمة المرور خاطئه '
                    });
                }

                // if (!user.isActivated) {
                //     return res.status(402).send({
                //         activationCode: user.latestActivationCode,
                //         message: 'حسابك غير مفعل  '
                //     })
                // }

                res.send({
                    token: createJWT(user),
                    user: user
                });
            });
        });
    });

    // register
    router.post('/users/register', function (req, res) {
        User.find({
            email: req.body.email
        }, function (err, docs) {
            if (docs.length) {
                return res.status(409).send({
                    message: 'هذا البريد الإلكتروني موجود بالفعل '
                });
            } else {
                let user = new User(req.body);
                let activationCode = UserHelper.makeActivationCode();
                UserHelper.sendEmail(user.email, user.name, activationCode);
                user.latestActivationCode = activationCode;
                user.save(function (err, docs) {
                    if (err) {
                        console.log(err);
                    }

                   return res.send({
                        activationCode: user.latestActivationCode
                    });
                });
            }
        });
    });



     // register
     router.post('/users/mobregister', function (req, res) {
        User.find({
            email: req.body.email
        }, function (err, docs) {
            if (docs.length) {
                return res.status(409).send({
                    message: 'هذا البريد الإلكتروني موجود بالفعل '
                });
            } else {
                let user = new User(req.body);
                //let activationCode = UserHelper.makeActivationCode();
                //UserHelper.sendEmail(user.email, user.name, activationCode);
                user.latestActivationCode = "123456";
                user.save(function (err, docs) {
                    if (err) {
                        console.log(err);
                    }

                   return res.send({
                        activationCode: "123456"
                    });
                });
            }
        });
    });



    router.post('/users/updateprofile', ensureAuthenticated, function (req, res) {
        console.log(res.body);

        User.findById(req.body._id, function (err, doc) {
            if (err) {
                return res.status(500).send(err);
            }
            User.updateOne(doc, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    // newPassword , oldPassword
    router.post('/users/changepassword', ensureAuthenticated, function (req, res) {
        User.findOne({
            email: req.body.email
        }, function (err1, user) {
            console.log(user);
            if (err1) {
                return res.status(500).send(err);
            }
            user.comparePassword(req.body.oldPassword, function (err, isMatch) {
                console.log(isMatch);
                if (!isMatch) {
                    return res.status(401).send({
                        message: 'كلمة المرور خاطئه '
                    });
                }
                user.password = req.body.newPassword;
                user.save(function (err, docs) {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    res.status(200).send({
                        token: createJWT(user),
                        user: user
                    })
                });
            });
        });
    });


    router.post('/users/forgetpassword', function (req, res) {
        User.findOne({
            email: req.body.email
        }, function (err1, user) {
            if (err1) {
                return res.status(500).send(err);
            }

            if (user == null) {
                return res.status(204).send({ message: 'عفوا البريد الالكترونى غير مستخدم' });
            }

            let activationCode = UserHelper.makeActivationCode();
            UserHelper.sendEmail(user.email, user.name, activationCode);
            user.latestActivationCode = activationCode;
            user.save(function (err, docs) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.status(200).send({
                    activationCode: activationCode
                })
            });
        })
    });

    router.post('/users/resetpassword', function (req, res) {
        User.findOne({
            email: req.body.email
        }, function (err1, user) {
            if (err1) {
                return res.status(500).send(err);
            }
            user.password = req.body.password;
            user.save(function (err, docs) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.status(200).send({
                    token: createJWT(user),
                    user: user
                })
            });
        });
    });


    router.post('/users/activateAccount', function (req, res) {
        User.findOne({
            email: req.body.email
        }, function (err1, user) {
            if (err1) {
                return res.status(500).send(err);
            }
            user.isActivated = true;
            user.save(function (err, docs) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.status(200).send({
                    token: createJWT(user),
                    user: user
                })
            });
        });
    });

}