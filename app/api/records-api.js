const records = require('../models/record');

module.exports = function (router, ensureAuthenticated) {


    router.get('/records/all', function (req, res) {
        records.find({}, function (err, docs) {
            res.send(docs);
        });
    });



    router.post('/records/add', ensureAuthenticated, function (req, res) {
        records.remove({},function(err,docs){
            if(err)
            console.log(err);


        });
       records.insertMany([req.body], function (err, docs) {
           if (err) {
               return res.status(500).send(err);
           }
           res.send(docs);
       })
    });


    router.post('/records/update', ensureAuthenticated, function (req, res) {
        records.findById(req.body._id, function (err, ch) {
            if (err) {
                return res.status(500).send(err);
            }
            records.updateOne(ch, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });



}