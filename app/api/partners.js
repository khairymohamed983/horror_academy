const partners = require('../models/partner');

module.exports = function (router, ensureAuthenticated) {
    router.get('/partners/all', function (req, res) {
        partners
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });

    router.post('/partners/add', ensureAuthenticated, function (req, res) {
        partners.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });



    router.get('/gallry/partners/newall', function (req, res) {
        var perPage = req.param('perPage'), page = req.param('page');
        let ammount=parseInt(page)*parseInt(perPage);

        var query = partners.find().skip(ammount).limit(parseInt( perPage));
        
        query.exec(function (err, docs) {
            res.send({
                partners: docs,
                page: page
            });
        });
    });



    router.post('/partners/update', ensureAuthenticated, function (req, res) {
        partners.findById(req.body._id, function (err, doc) {
            if (err) {
                return res.status(500).send(err);
            }
            partners.updateOne(doc, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    router.post('/partners/delete', ensureAuthenticated, function (req, res) {
        partners.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });

}
