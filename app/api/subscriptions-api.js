﻿const subscriptions = require('../models/newsubscription');

module.exports = function (router, ensureAuthenticated) {
    router.get('/subscriptions/all', ensureAuthenticated,function (req, res) {
        subscriptions
            .find({}, function (err, doc) {
                res.send(doc);
            });
    });



    router.get('/subscriptions/byStatus',  ensureAuthenticated, function (req, res) {       
        console.log(req.query.status);       
        subscriptions.aggregate(
            [
                { $match: { "status": req.query.status } },
            ]
            , function (err, docs) {
                if (err) {
                    res.status(500).send(err);
                 }
                 res.send(docs);
            })
    })

    //router.get('/subscriptions/byStatus', function (req, res) {
    //console.log(req.query.status);       
    //    subscriptions.find({ "status": req.query.status }
    //        , function (err, docs) {
    //            if (err) {
    //                res.status(500).send(err);
     //            }
     //            res.send(docs);
    //        })
    //})


    router.post('/subscriptions/add',function (req, res) {
        subscriptions.
            insertMany([req.body], function (err, docs) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(docs);
            });
    });


    router.post('/subscriptions/update', ensureAuthenticated, function (req, res) {
        subscriptions.findById(req.body._id, function (err, docs) {
            if (err) {
                res.status(500).send;
            }
            subscriptions.updateOne(docs, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            })
        });
    });


    router.post('/subscriptions/delete', ensureAuthenticated, function (req, res) {
        subscriptions.deleteOne(req.body, function (err) {
            if (err) {
                res.status(500).send(err);
            }
            res.status(200).send();
        })
    });


}

