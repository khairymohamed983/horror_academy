const video = require('../models/video');
const image = require('../models/image');

module.exports = function (router, ensureAuthenticated) {

    // for images =================================================================
    router.get('/gallry/images/newall', function (req, res) {


        var perPage = req.param('perPage'), page = req.param('page');
        let ammount=parseInt(page)*parseInt(perPage);

        var query = image.find().skip(ammount).limit(parseInt( perPage));
        
        query.exec(function (err, docs) {
            res.send({
                images: docs,
                page: page
            });
        });
    });


    router.get('/gallry/images/all', function (req, res) {
        image.find({}, function (err, docs) {
            res.send(docs);
        });
    });



    router.post('/gallry/images/add', ensureAuthenticated, function (req, res) {
        image.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/gallry/images/update', ensureAuthenticated, function (req, res) {
        image.findById(req.body._id, function (err, doc) {
            if (err) {
                return res.status(500).send(err);
            }
            image.updateOne(doc, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });


    router.post('/gallry/images/delete', ensureAuthenticated, function (req, res) {
        image.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });


    // for videos =================================================================
    router.get('/gallry/videos/all', function (req, res) {
        video
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });




    router.get('/gallry/videos/newall', function (req, res) {
        var perPage = req.param('perPage'), page = req.param('page');
        let ammount=parseInt(page)*parseInt(perPage);

        var query = video.find().skip(ammount).limit(parseInt( perPage));
        
        query.exec(function (err, docs) {
            res.send({
                videos: docs,
                page: page
            });
        });
    });


    router.post('/gallry/videos/add', ensureAuthenticated, function (req, res) {
        video.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/gallry/videos/update', ensureAuthenticated, function (req, res) {
        video.findById(req.body._id, function (err, doc) {
            if (err) {
                return res.status(500).send(err);
            }
            video.updateOne(doc, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });


    router.post('/gallry/videos/delete', ensureAuthenticated, function (req, res) {
        video.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });
}