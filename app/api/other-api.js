

const about = require('../models/about');
const contact = require('../models/contactInfo');
const live = require('../models/live');
const message = require('../models/message');
const championship = require('../models/championships');
const commision = require('../models/commission');
const image = require('../models/image');
const news = require('../models/news');
const partner = require('../models/partner');
const player = require('../models/player');
const video = require('../models/video');
const user = require('../models/user')
const joinus = require('../models/joinus')
const subscriptions = require('../models/subscription');
const newsubscriptions = require('../models/newsubscription');




module.exports = function (router, ensureAuthenticated) {

    router.get('/other/contact', function (req, res) {
        contact
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });

    router.get('/other/statistics', function (req, res) {
        try {
            let applications = 0;
            player.count({}, function (err, players) {
                championship.count({}, function (err, championships) {
                    news.count({}, function (err, newses) {
                        partner.count({}, function (err, partners) {
                            message.count({}, function (err, messages) {
                                image.count({}, function (err, images) {
                                    video.count({}, function (err, videos) {
                                        user.count({}, function (err, users) {
                                            return res.status(200).send({
                                                applications,
                                                players,
                                                championships,
                                                newses,
                                                partners,
                                                messages,
                                                users,
                                                gallary: videos + images
                                            });
                                        })
                                    })
                                })
                            })
                        });
                    });
                });
            });
        } catch (err) {
            res.status(500).send(err);
        }
    });

    router.get('/other/about', function (req, res) {
        about.find({}, function (err, docs) {
            res.send(docs);
        });
    });




    router.post('/other/saveAbout', ensureAuthenticated, function (req, res) {

        about.remove({}, function (err, docs) {
            if (err)
                console.log(err);
        });

        about.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/other/saveContact', ensureAuthenticated, function (req, res) {
        contact.findById(req.body._id, function (err, doc) {
            if (err) {
                return res.status(500).send(err);
            }
            contact.updateOne(doc, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    router.post('/other/live/add', ensureAuthenticated, function (req, res) {
        live.update({
            isCurrent: true
        }, {
                isCurrent: false
            }, {
                multi: true
            }, function (err, rows) {
                console.log(err)
                if (err) {
                    return res.status(500).send();
                }
                live.insertMany([req.body], function (ierr, irows) {
                    console.log(ierr)

                    if (ierr) {
                        return res.status(500).send();
                    }
                    return res.status(200).send(irows);
                })
            });
    });

    router.get('/other/live/history', ensureAuthenticated, function (req, res) {
        live
            .find({
                isCurrent: false
            }, function (err, docs) {
                res.send(docs);
            });
    });

    router.get('/other/live/', ensureAuthenticated, function (req, res) {
        live
            .find({
                isCurrent: true
            }, function (err, docs) {
                res.send(docs);
            });
    });



    router.get('/other/contactMessages/all', ensureAuthenticated, function (req, res) {
        message
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });

    router.post('/other/contactMessages/add', function (req, res) {
        message.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/other/contactMessages/delete', ensureAuthenticated, function (req, res) {
        message.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });


    // join us

    router.post('/other/joinus', function (req, res) {

        let subscripreobj = {
            name: req.body.name,
            email: req.body.email,
            mobile: req.body.mobile,
            address: req.body.address,
            district: req.body.region,
            birthdate: req.body.birthdate,
            description: req.body.reason,
            sourceKnowing: req.body.howknow,
            status: "pending"
        }
        console.log("body: " +JSON.stringify(req.body));

        console.log("obj:" + JSON.stringify(subscripreobj));

        newsubscriptions.
            insertMany(subscripreobj, function (err, docs) {
                if (err) {
                    console.log("add sub error:" +JSON.stringify( err));
                }
                res.send(docs);
            });

            
    });


    router.post('/other/joinus/all', function (req, res) {
        joinus.find({}, function (err, docs) {
            res.send(docs);
        });
    });





}
