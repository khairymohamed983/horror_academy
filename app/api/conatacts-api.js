﻿const contacts = require('../models/contactInfo');
var mongoose = require('mongoose');

module.exports = function (router, ensureAuthenticated) {
    router.get('/contacts/all', function (req, res) {
        contacts
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });

    router.post('/contacts/add', ensureAuthenticated, function (req, res) {

        contacts.remove({},function(err,docs){
            if(err)
            console.log(err);
        });

        contacts.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });


    router.post('/contacts/update', ensureAuthenticated, function (req, res) {
        contacts.findById(req.body._id, function (err, ch) {
            if (err) {
                return res.status(500).send(err);
            }
            contacts.updateOne(ch, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    router.post('/contacts/delete', ensureAuthenticated, function (req, res) {
        contacts.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });

}
