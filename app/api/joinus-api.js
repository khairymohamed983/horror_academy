const joinus = require('../models/joinus');
var mongoose = require('mongoose');

module.exports = function (router, ensureAuthenticated) {

    router.get('/joinus/all', function (req, res) {
        joinus
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });

    router.post('/joinus/add', function (req, res) {
        joinus.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

}