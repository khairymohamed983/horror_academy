const champions = require('../models/championships');
var mongoose = require('mongoose');

module.exports = function (router, ensureAuthenticated) {
    router.get('/champions/all', function (req, res) {
        champions
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });


    router.get('/champions/newall', function (req, res) {

        let perPage = req.param('perPage');;
        let  page = req.param('page');

        let ammount=parseInt(perPage)*parseInt(page);
        var query = champions.find().skip(ammount).limit(parseInt(perPage));
        query.exec(function (err, docs) {
            res.send({
                champions: docs,
                page: page
            });
        });

    });


    router.post('/champions/add', ensureAuthenticated, function (req, res) {
        champions.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/champions/update', ensureAuthenticated, function (req, res) {
        // var id = mongoose.Types.ObjectId(req.body._id);
        // champions.findById(id, function (err, ch) {
        //     if (err) {
        //         return res.status(500).send(err);
        //     }
        //     ch.image = req.body.image;
        //     ch.title = req.body.title;
        //     ch.year = req.body.year;
        //     ch.description = req.body.description;
        //     ch.save(function (err, updatedTank) {
        //         if (err) {
        //             return res.status(500).send(err);
        //         }
        //         res.send(updatedTank);
        //     });
        // })

        champions.findById(req.body._id,function (err, ch) {
            if (err) {
                return res.status(500).send(err);
            }
            champions.updateOne(ch, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    router.post('/champions/delete', ensureAuthenticated, function (req, res) {
        champions.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });
}
