const news = require('../models/news');

module.exports = function (router, ensureAuthenticated) {
    router.get('/news/all', function (req, res) {
        news
            .find({}, function (err, docs) {
                res.send(docs);
            });
    });


    router.get('/news/newall', function (req, res) {

        let perPage = req.param('perPage');;
        let page = req.param('page');
        let ammount = parseInt(perPage) * parseInt(page);

        var query = news.find().sort({'_id': -1}).skip(ammount).limit(parseInt(perPage));
        query.exec(function (err, docs) {
            res.send({
                news: docs,
                page: page
            });
        });

    });



    router.post('/news/add', ensureAuthenticated, function (req, res) {
        news.insertMany([req.body], function (err, docs) {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(docs);
        })
    });

    router.post('/news/update', ensureAuthenticated, function (req, res) {
        news.findById(req.body._id, function (err, doc) {
            if (err) {
                return res.status(500).send(err);
            }
            news.updateOne(doc, req.body, function (err2, res2) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.send(res2);
            });
        })
    });

    router.post('/news/delete', ensureAuthenticated, function (req, res) {
        news.deleteOne(req.body, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });

}
