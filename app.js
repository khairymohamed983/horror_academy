const path = require('path');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const http = require('http');
const app = express();
const mongoose = require('mongoose');
const config = require('./config/config');
const opn = require('opn')
var mongoOptions = {
    server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } }
};

mongoose.connect(config.mongoUri, {});

app.use(cors());

//parse application/json
app.use(bodyParser.json({
    extended: true,
    limit: '5mb'
}));

//parse application/vnd.api+json as json
app.use(bodyParser.json({
    type: 'application/vnd.api+json',
    limit: '5mb'
}));

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '5mb'
}));

//override with X-HTTP-Method-override header in the request
app.use(methodOverride('X-HTTP-Method-Override'));

// Force HTTPS on Heroku
if (app.get('env') === 'production') {
    app.use(function (req, res, next) {
        var protocol = req.get('x-forwarded-proto');
        protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
    });
}

// Static Content Images Videos etx .. 
// app.use('/static', express.static(path.join(__dirname, 'static')));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'admin')));

// API
const api = require('./app/bundle')();
app.use('/api', api);


// Send all other requests to the admin
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'admin/index.html'));
});

// Seed Data into database
require('./app/seeder')();

//Set Port
const port = process.env.PORT || '1994';
app.set('port', port);

const server = http.createServer(app);
console.log('we are here ');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log('connected');
    server.listen(port, () => {
        console.log(`Running on localhost:${port}`)
        opn(`http://localhost:${port}`, { app: ['chrome'] });
    });
});
exports = module.exports = app;